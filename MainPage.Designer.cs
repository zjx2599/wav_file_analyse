﻿namespace wav文件提取转换
{
    partial class MainPage
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddFiles = new System.Windows.Forms.Button();
            this.listFiles = new System.Windows.Forms.ListView();
            this.fileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.filePath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteFile = new System.Windows.Forms.Button();
            this.grpFileOpt = new System.Windows.Forms.GroupBox();
            this.btnPlayStop = new System.Windows.Forms.Button();
            this.btnPlaySound = new System.Windows.Forms.Button();
            this.btnExportAllData = new System.Windows.Forms.Button();
            this.btnExportData = new System.Windows.Forms.Button();
            this.btnClearFiles = new System.Windows.Forms.Button();
            this.grpFileDetails = new System.Windows.Forms.GroupBox();
            this.listDetails = new System.Windows.Forms.ListView();
            this.attrItem = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.attrValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpFileList = new System.Windows.Forms.GroupBox();
            this.grpFileOpt.SuspendLayout();
            this.grpFileDetails.SuspendLayout();
            this.grpFileList.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddFiles
            // 
            this.btnAddFiles.Location = new System.Drawing.Point(6, 20);
            this.btnAddFiles.Name = "btnAddFiles";
            this.btnAddFiles.Size = new System.Drawing.Size(63, 23);
            this.btnAddFiles.TabIndex = 0;
            this.btnAddFiles.Text = "添加文件";
            this.btnAddFiles.UseVisualStyleBackColor = true;
            this.btnAddFiles.Click += new System.EventHandler(this.btnAddFiles_Click);
            // 
            // listFiles
            // 
            this.listFiles.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.listFiles.AutoArrange = false;
            this.listFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.fileName,
            this.filePath});
            this.listFiles.FullRowSelect = true;
            this.listFiles.GridLines = true;
            this.listFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listFiles.HideSelection = false;
            this.listFiles.Location = new System.Drawing.Point(6, 20);
            this.listFiles.Name = "listFiles";
            this.listFiles.OwnerDraw = true;
            this.listFiles.Size = new System.Drawing.Size(463, 562);
            this.listFiles.TabIndex = 1;
            this.listFiles.UseCompatibleStateImageBehavior = false;
            this.listFiles.View = System.Windows.Forms.View.Details;
            this.listFiles.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listView_DrawColumnHeader);
            this.listFiles.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView_DrawItem);
            this.listFiles.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listView_DrawSubItem);
            this.listFiles.SelectedIndexChanged += new System.EventHandler(this.listFiles_SelectedIndexChanged);
            this.listFiles.DoubleClick += new System.EventHandler(this.listFiles_DoubleClick);
            this.listFiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listFiles_KeyDown);
            // 
            // fileName
            // 
            this.fileName.Text = "文件名";
            this.fileName.Width = 69;
            // 
            // filePath
            // 
            this.filePath.Text = "文件目录";
            this.filePath.Width = 393;
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.Location = new System.Drawing.Point(75, 20);
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.Size = new System.Drawing.Size(85, 23);
            this.btnDeleteFile.TabIndex = 2;
            this.btnDeleteFile.Text = "移除选中文件";
            this.btnDeleteFile.UseVisualStyleBackColor = true;
            this.btnDeleteFile.Click += new System.EventHandler(this.btnDeleteFile_Click);
            // 
            // grpFileOpt
            // 
            this.grpFileOpt.Controls.Add(this.btnPlayStop);
            this.grpFileOpt.Controls.Add(this.btnPlaySound);
            this.grpFileOpt.Controls.Add(this.btnExportAllData);
            this.grpFileOpt.Controls.Add(this.btnExportData);
            this.grpFileOpt.Controls.Add(this.btnClearFiles);
            this.grpFileOpt.Controls.Add(this.btnAddFiles);
            this.grpFileOpt.Controls.Add(this.btnDeleteFile);
            this.grpFileOpt.Location = new System.Drawing.Point(12, 606);
            this.grpFileOpt.Name = "grpFileOpt";
            this.grpFileOpt.Size = new System.Drawing.Size(834, 48);
            this.grpFileOpt.TabIndex = 3;
            this.grpFileOpt.TabStop = false;
            this.grpFileOpt.Text = "文件操作";
            // 
            // btnPlayStop
            // 
            this.btnPlayStop.Location = new System.Drawing.Point(765, 20);
            this.btnPlayStop.Name = "btnPlayStop";
            this.btnPlayStop.Size = new System.Drawing.Size(63, 23);
            this.btnPlayStop.TabIndex = 7;
            this.btnPlayStop.Text = "停止播放";
            this.btnPlayStop.UseVisualStyleBackColor = true;
            this.btnPlayStop.Click += new System.EventHandler(this.btnPlayStop_Click);
            // 
            // btnPlaySound
            // 
            this.btnPlaySound.Location = new System.Drawing.Point(698, 20);
            this.btnPlaySound.Name = "btnPlaySound";
            this.btnPlaySound.Size = new System.Drawing.Size(61, 23);
            this.btnPlaySound.TabIndex = 6;
            this.btnPlaySound.Text = "播放文件";
            this.btnPlaySound.UseVisualStyleBackColor = true;
            this.btnPlaySound.Click += new System.EventHandler(this.btnPlaySound_Click);
            // 
            // btnExportAllData
            // 
            this.btnExportAllData.Location = new System.Drawing.Point(396, 20);
            this.btnExportAllData.Name = "btnExportAllData";
            this.btnExportAllData.Size = new System.Drawing.Size(134, 23);
            this.btnExportAllData.TabIndex = 5;
            this.btnExportAllData.Text = "导出所有文件音频数据";
            this.btnExportAllData.UseVisualStyleBackColor = true;
            this.btnExportAllData.Click += new System.EventHandler(this.btnExportAllData_Click);
            // 
            // btnExportData
            // 
            this.btnExportData.Location = new System.Drawing.Point(257, 20);
            this.btnExportData.Name = "btnExportData";
            this.btnExportData.Size = new System.Drawing.Size(133, 23);
            this.btnExportData.TabIndex = 4;
            this.btnExportData.Text = "导出选中文件音频数据";
            this.btnExportData.UseVisualStyleBackColor = true;
            this.btnExportData.Click += new System.EventHandler(this.btnExportData_Click);
            // 
            // btnClearFiles
            // 
            this.btnClearFiles.Location = new System.Drawing.Point(166, 20);
            this.btnClearFiles.Name = "btnClearFiles";
            this.btnClearFiles.Size = new System.Drawing.Size(85, 23);
            this.btnClearFiles.TabIndex = 3;
            this.btnClearFiles.Text = "移除所有文件";
            this.btnClearFiles.UseVisualStyleBackColor = true;
            this.btnClearFiles.Click += new System.EventHandler(this.btnClearFiles_Click);
            // 
            // grpFileDetails
            // 
            this.grpFileDetails.Controls.Add(this.listDetails);
            this.grpFileDetails.Location = new System.Drawing.Point(495, 12);
            this.grpFileDetails.Name = "grpFileDetails";
            this.grpFileDetails.Size = new System.Drawing.Size(351, 588);
            this.grpFileDetails.TabIndex = 4;
            this.grpFileDetails.TabStop = false;
            this.grpFileDetails.Text = "文件详细信息";
            // 
            // listDetails
            // 
            this.listDetails.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.listDetails.AutoArrange = false;
            this.listDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.attrItem,
            this.attrValue});
            this.listDetails.FullRowSelect = true;
            this.listDetails.GridLines = true;
            this.listDetails.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listDetails.HideSelection = false;
            this.listDetails.LabelWrap = false;
            this.listDetails.Location = new System.Drawing.Point(6, 20);
            this.listDetails.Name = "listDetails";
            this.listDetails.OwnerDraw = true;
            this.listDetails.Size = new System.Drawing.Size(337, 562);
            this.listDetails.TabIndex = 0;
            this.listDetails.UseCompatibleStateImageBehavior = false;
            this.listDetails.View = System.Windows.Forms.View.Details;
            this.listDetails.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listView_DrawColumnHeader);
            this.listDetails.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView_DrawItem);
            this.listDetails.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listView_DrawSubItem);
            // 
            // attrItem
            // 
            this.attrItem.Text = "项目";
            this.attrItem.Width = 112;
            // 
            // attrValue
            // 
            this.attrValue.Text = "值";
            this.attrValue.Width = 225;
            // 
            // grpFileList
            // 
            this.grpFileList.Controls.Add(this.listFiles);
            this.grpFileList.Location = new System.Drawing.Point(12, 12);
            this.grpFileList.Name = "grpFileList";
            this.grpFileList.Size = new System.Drawing.Size(477, 588);
            this.grpFileList.TabIndex = 5;
            this.grpFileList.TabStop = false;
            this.grpFileList.Text = "文件列表";
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 666);
            this.Controls.Add(this.grpFileList);
            this.Controls.Add(this.grpFileDetails);
            this.Controls.Add(this.grpFileOpt);
            this.Name = "MainPage";
            this.Text = "wav文件提取分析工具";
            this.Load += new System.EventHandler(this.MainPage_Load);
            this.grpFileOpt.ResumeLayout(false);
            this.grpFileDetails.ResumeLayout(false);
            this.grpFileList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddFiles;
        private System.Windows.Forms.ListView listFiles;
        private System.Windows.Forms.ColumnHeader fileName;
        private System.Windows.Forms.ColumnHeader filePath;
        private System.Windows.Forms.Button btnDeleteFile;
        private System.Windows.Forms.GroupBox grpFileOpt;
        private System.Windows.Forms.GroupBox grpFileDetails;
        private System.Windows.Forms.ListView listDetails;
        private System.Windows.Forms.ColumnHeader attrItem;
        private System.Windows.Forms.ColumnHeader attrValue;
        private System.Windows.Forms.Button btnExportData;
        private System.Windows.Forms.Button btnClearFiles;
        private System.Windows.Forms.GroupBox grpFileList;
        private System.Windows.Forms.Button btnExportAllData;
        private System.Windows.Forms.Button btnPlaySound;
        private System.Windows.Forms.Button btnPlayStop;
    }
}

